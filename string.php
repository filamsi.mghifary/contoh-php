<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Soal</h2>
    <?php

        echo "<h3>Contoh 1</h3>"; 
        $kalimat1 = "Hello World";
        echo "Kalimat Pertama : ". $kalimat1 . "<br>";
        echo "Panjang String : " . strlen($kalimat1). "<br>";
        echo "Jumlah Kata : ". str_word_count($kalimat1) . "<br><br>";


        echo "<h3>Contoh 2</h3>";
        $string2 = "Nama Saya Filamsi";
        echo "Kalimat Kedua : " . $string2."<br>";
        echo "Kata Pertama : " . substr($string2,0,4) . "<br>";
        echo "Kata Kedua : " . substr($string2,5,4) . "<br>";
        echo "Kata Ketiga : " . substr($string2,10,7) . "<br>";

        echo "<h3>Contoh 3</h3>";
        $string3 = "Selamat Pagi";
        echo "Kalimat Ketiga : ". $string3."<br>";
        echo "Ganti Kalimat Ketiga : ". str_replace("Pagi","Malam",$string3);

    ?>
</body>
</html>